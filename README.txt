 TrackIt is a software for tracking and analysis of single-molecule
 fluorescence microscopy movies. The latest version of the software can 
 be found at 'https://gitlab.com/GebhardtLab/TrackIt'.

 Requirements:
 - Windows 10 64-bit
 - Matlab version 2017b and above
 - For full functionality, the following Matlab toolboxes are required: Optimization, Image
   Processing, Statistics and Machine Learning, Parallel Computing

 To start the main TrackIt GUI open TrackIt_v1_6.m with Matlab and press run (F5)
 The track analysis tool can be used independent from the main GUI by opening "track_analysis_tool.m" with Matlab and pressing run (F5)

 ========================================================================= 
 Publication:
 Single molecule tracking and analysis framework including theory-predicted parameter settings. Sci Rep 11, 9465 (2021)

 Copyright (C) 2024 Timo Kuhn, Johannes Hettich, Jonas Coßmann and J. Christof M. Gebhardt
 
 E-mail: 
 christof.gebhardt@uni-ulm.de
 =========================================================================

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or any
 later version.  This program is distributed in the hope that it will
 be useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 the GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.

 This product includes external software, see copyright in individual folders and  functions.

